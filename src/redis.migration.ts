import RedisService, {REDIS_KEYS} from './packages/shared/databases/redis.service';

const whiteListedIps = [
    '127.0.0.1',
    'localhost',
    '221.221.221.221'
];

RedisService.set(REDIS_KEYS.WHITELISTED_DOMAINS, JSON.stringify(whiteListedIps)).then(() => {
    RedisService.end(false)
});