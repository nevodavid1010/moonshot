import 'reflect-metadata';
import * as bodyParser from 'body-parser';
import { InversifyExpressServer } from 'inversify-express-utils';
import './packages/controllers.list';
import {myContainer} from './packages/container';


// create server
let server = new InversifyExpressServer(myContainer);
server.setConfig((app) => {
    // add body parser
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
});

let app = server.build();
app.listen(3000);
console.log('start listening on 3000');
