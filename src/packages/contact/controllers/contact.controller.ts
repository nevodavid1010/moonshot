import {controller, httpGet, httpPost, requestBody, queryParam} from 'inversify-express-utils';
import {ContactService} from '../services/contact.service';
import {inject} from 'inversify';
import {PlainValidationService} from '../../shared/validation/plain.validation.service';
import {ContactCreateValidator} from '../validators/contact.create.validator';
import {ContactListValidator} from '../validators/contact.list.validator';
import * as RateLimit from 'express-rate-limit';
import {DomainWhitelistMiddleware} from '../../shared/whitelist/domain.whitelist.middleware';

/**
 * Contact Controller
 */
@controller('/contact', DomainWhitelistMiddleware)
export class ContactController {
    constructor(@inject(ContactService) private _contactService: ContactService) {}

    /**
     * Put the rate limiter for creating a new contact
     * Check for validation and create a new contact
     * @param params
     */
    @httpPost('/',
        // @ts-ignore
        RateLimit({
            max: 1,
            windowMs: 3000
    }))
    async createContact(@requestBody() params: object) {
        const paramsVal = await PlainValidationService.testAndConvert<ContactCreateValidator>(params, ContactCreateValidator);
        if (paramsVal.errors.length) {
            return paramsVal.errors;
        }

        return this._contactService.createContact(paramsVal.convertedClass);
    }

    /**
     * Get contact list, in case of no contact return 500 (kinda weird usually you should return 404)
     * @param params
     */
    @httpGet('/')
    async getContactList(@queryParam() params: object) {
        const paramsVal = await PlainValidationService.testAndConvert<ContactListValidator>(params, ContactListValidator);
        if (paramsVal.errors.length) {
            return paramsVal.errors;
        }

        const contactList = await this._contactService.getContactList(paramsVal.convertedClass);
        if (!contactList.length) {
            return 500;
        }

        return contactList;
    }
}
