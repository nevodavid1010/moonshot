import {injectable} from 'inversify';
import Contact from '../models/contact.model';
import {ContactCreateValidator} from '../validators/contact.create.validator';
import {ContactListValidator} from '../validators/contact.list.validator';

@injectable()
export class ContactRepository {
    createContact(params: ContactCreateValidator) {
        // in case user is sending a json, convert it to string
        const contactModel = new Contact(Object.keys(params).reduce((allObj, current) => ({
                    ...allObj,
                    [current]: ['date', 'domain'].indexOf(current) === -1 && typeof params[current] === 'object' ? JSON.stringify(params[current]) : params[current]
                }), {}
        ));

        return contactModel.save();
    }

    getContactList(params: ContactListValidator) {
        return Contact.aggregate([{
            $match: {
                ...params.website ? {domain: params.website} : {},
                ...(!params.from && !params.until) ? {} : {
                    date: {
                        ...params.from ? {
                            $gte : new Date(params.from)
                        } : {},
                        ...params.until ? {
                            $lt: new Date(params.until)
                        } : {}
                    }
                }
            }
        }, {
            $skip: params.skip
        }, {
            $limit: params.limit
        }]);
    }
}
