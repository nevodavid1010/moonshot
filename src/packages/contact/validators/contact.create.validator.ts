import {IsString} from 'class-validator';
import {IsValidDate} from '../../shared/validation/valid.date';

export class ContactCreateValidator {
    @IsString()
    domain: string;

    @IsValidDate()
    date: string;
}
