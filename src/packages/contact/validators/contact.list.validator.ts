import {IsNumber, IsOptional, IsString, Min} from 'class-validator';
import {IsValidDate} from '../../shared/validation/valid.date';
import {Transform} from 'class-transformer';

export class ContactListValidator {
    @IsString()
    @IsOptional()
    website: string;

    @IsNumber()
    @Min(0)
    @Transform((num) => Number(num))
    skip: number = 0;

    @IsNumber()
    @Min(1)
    @Transform((num) => Number(num))
    limit: number = 20;

    @IsOptional()
    @IsValidDate()
    from: string;

    @IsOptional()
    @IsValidDate()
    until: string;
}
