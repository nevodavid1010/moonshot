import {inject, injectable} from 'inversify';
import {ContactRepository} from '../repositories/contact.repository';
import {ContactCreateValidator} from '../validators/contact.create.validator';
import {ContactListValidator} from '../validators/contact.list.validator';

@injectable()
export class ContactService {
    constructor(@inject(ContactRepository) private _contactRepository: ContactRepository) {}

    createContact(params: ContactCreateValidator) {
        return this._contactRepository.createContact(params);
    }

    getContactList(params: ContactListValidator) {
        return this._contactRepository.getContactList(params);
    }
}