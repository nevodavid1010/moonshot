import {Document, Schema} from 'mongoose';
import * as mongoose from "mongoose";

export interface ContactInterface extends Document {
    domain: string,
    date: string
}

/**
 * Defined the contact model, the domain and date are required, we are using non strict mode so we can add more params
 */
const Contact = mongoose.model<ContactInterface>('contact', new Schema({
    domain: {required: true, type: String},
    date: {required: true, type: Date}
}, {strict: false}));

export default Contact;
