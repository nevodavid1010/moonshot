import {MongoService} from './shared/databases/mongo.service';
import {ContactService} from './contact/services/contact.service';
import {ContactRepository} from './contact/repositories/contact.repository';
import {DomainWhitelist} from './shared/whitelist/domain.whitelist';
import {DomainWhitelistMiddleware} from './shared/whitelist/domain.whitelist.middleware';


export const SERVICES =
    {MongoService, ContactService, ContactRepository, DomainWhitelist, DomainWhitelistMiddleware}
;
