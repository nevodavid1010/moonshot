// set up container
import {Container} from 'inversify';
import {SERVICES} from './services.list';

const myContainer = new Container();
Object.keys(SERVICES).map((service) => {
    myContainer.bind(SERVICES[service]).to(SERVICES[service]);
});

export {myContainer};