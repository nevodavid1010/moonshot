import {registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface} from 'class-validator';
import * as moment from 'moment';

@ValidatorConstraint({ async: true })
export class ValidDate implements ValidatorConstraintInterface {

    validate(date: string, args: ValidationArguments) {
        return moment(date, 'YYYY-MM-DD HH:mm:ss', true).isValid();
    }

}

export function IsValidDate(validationOptions: ValidationOptions = {}) {
    return (object: object, propertyName: string) => {
        validationOptions.message = 'Invalid Date';
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: ValidDate
        });
    };
}