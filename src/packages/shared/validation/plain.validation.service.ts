import {plainToClass} from 'class-transformer';
import {validate} from 'class-validator';
export class PlainValidationService {
    static async testAndConvert<T>(object: any, classObj) {
        const convertedClass =  plainToClass(classObj, object);
        return {errors: (await validate(convertedClass)).map(e => e.constraints), convertedClass: (convertedClass.length ? convertedClass[0] : convertedClass) as T};
    }
}