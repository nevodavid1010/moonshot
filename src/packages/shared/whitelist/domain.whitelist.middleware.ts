import {BaseMiddleware} from "inversify-express-utils";
import {inject, injectable} from 'inversify';
import {DomainWhitelist} from './domain.whitelist';
import * as express from 'express';

@injectable()
export class DomainWhitelistMiddleware extends BaseMiddleware {
    @inject(DomainWhitelist) private readonly _domainWhitelist: DomainWhitelist;
    public async handler(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
       const ip = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress;

       if (!await this._domainWhitelist.checkIpOrDomain(ip as string)) {
           res.status(403).send('Invalid ip');
           return ;
       }

       next();
    }
}