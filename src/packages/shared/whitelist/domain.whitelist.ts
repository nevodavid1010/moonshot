import RedisService, {REDIS_KEYS} from '../databases/redis.service';
import {injectable} from 'inversify';

@injectable()
export class DomainWhitelist {
    async checkIpOrDomain(domainOrIp: string) {
        const whiteListedDomains = JSON.parse(await RedisService.get(REDIS_KEYS.WHITELISTED_DOMAINS) || '[]');
        // we are using some and not index of an array to find if it's containing the domain name
        return whiteListedDomains.some(listed => {
            return domainOrIp.indexOf(listed) !== -1;
        });
    }
}
