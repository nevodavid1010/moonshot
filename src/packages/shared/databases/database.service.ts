import {injectable} from 'inversify';

@injectable()
export abstract class DatabaseService {
    static connection;
    public static getConnection() {
        return DatabaseService.connection;
    }
}
