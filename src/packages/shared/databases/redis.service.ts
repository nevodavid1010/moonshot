import * as redis from 'redis';
import config from '../../../config';

export enum REDIS_KEYS {
    WHITELISTED_DOMAINS = 'WHITELISTED_DOMAINS'
}

const redisClient = redis.createClient(config.redisPort, {
    host: 'redis',
    db: 1
});

export class RedisService {
    static get(name: string): Promise<string> {
        return new Promise((res, rej) => {
            redisClient.get(name, (err, value) => {
                if (err) {
                    rej('error');
                }

                res(value);
            })
        });
    }

    static set(name: string, value: string) {
        return new Promise((res, rej) => {
            redisClient.set(name, value, (err, value) => {
                if (err) {
                    rej('error');
                }

                res(value);
            })
        });
    }

    static end(flush: boolean) {
        redisClient.end(flush);
    }
}

export default RedisService;
