import * as mongoose from 'mongoose';
import config from '../../../config';
import {injectable} from 'inversify';
import {DatabaseService} from './database.service';

@injectable()
export class MongoService extends DatabaseService {

}

// set the connection for the MongoService
MongoService.connection = mongoose.connect(config.mongoUrl, {
    useNewUrlParser: true,
    replicaSet: 'rs',
    autoReconnect: true
}).then(() => {
    console.log('connected');
});

