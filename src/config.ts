export interface PARAMS {
    mongoUrl: string;
    redisPort: number;
}

export interface PARAMS_ENV {
    development: PARAMS;
    stage: PARAMS;
    production: PARAMS;
}

const config: PARAMS_ENV =  {
    development: {
        mongoUrl: 'mongodb://mongo1:27017/moonshot',
        redisPort: 6379
    },
    stage: {
        mongoUrl: 'mongodb://mongo1:27017/moonshot',
        redisPort: 6379
    },
    production: {
        mongoUrl: 'mongodb://mongo1:27017/moonshot',
        redisPort: 6379
    }
};

export default config[process.env.NODE_ENV] ? config[process.env.NODE_ENV] : config.development;